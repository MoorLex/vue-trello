# Starter

> Starter description
> sudo npm install --save --unsafe-perm node-sass

## Build Setup

``` bash
# install dependencies
npm

# serve with hot reload at localhost:8080
npm dev

# build for production with minification
npm build

# build for production and view the bundle analyzer report
npm build --report

# run unit tests
npm unit

# run all tests
npm test
```
