import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({

  state: () => ({
    lists: []
  }),

  actions: {
    ADD_LIST: (context, props) => {
      context.commit('ADD_LIST', props);
    },
    ADD_ITEM: (context, props) => {
      context.commit('ADD_ITEM', props);
    },
  },

  mutations: {
    ADD_LIST: (state, {title, color}) => {
      state.lists.push({
        title,
        color,
        id: makeId(),
        items: []
      });
    },
    ADD_ITEM: (state, {id, name}) => {
      let list = state.lists.find(list => list.id === id);

      list.items.push({
        name,
        id: makeId()
      });
    }
  },

  getters: {
    LISTS: state => {
      return state.lists;
    },
    LIST: state => (id) => {
      return state.lists.find(list => list.id === id);
    }
  }
})

function makeId(length = 5) {
  let result = '';
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let charactersLength = characters.length;

  for ( let i = 0; i < length; i++ )
    result += characters.charAt(Math.floor(Math.random() * charactersLength));

  return result;
}
